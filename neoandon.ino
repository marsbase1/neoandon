//////////////////////////////////////////////////////////////////////////////////
//
// This application implements a visual alert system, (aka: "andon"), for monitoring
// factory equipment.
//
// The basic operation is to have a COTS Arduino board drive a Neopixel 8x8 matrix
// and display colors and patterns to represent different equipment status states.
//
// Wiring:
//
// D0  - nc
// D1  - nc
// D2  - nc
// D3  - nc 
// D4  - nc
// D5  - nc
// D6  - DO - WS2812B Drive
// D7  - nc
// D8  - nc
// D9  - nc
// D10 - nc
// D11 - nc
// D12 - nc
// D13 - nc
// A0  - Speed adj pot, (currently not used or implemented)
// A1  - nc
//
///////////////////////////////////////////////////////////////////////////
#include <Adafruit_NeoPixel.h>
#ifdef __AVR__
  #include <avr/power.h>
#endif

#define PIN 49

// Define the input pins, connected to the PLC control lines.
const int sw1in = 1;
const int sw2in = 2;
const int sw3in = 3;
const int sw4in = 4;


// Parameter 1 = number of pixels in strip
// Parameter 2 = Arduino pin number (most are valid)
// Parameter 3 = pixel type flags, add together as needed:
//   NEO_KHZ800  800 KHz bitstream (most NeoPixel products w/WS2812 LEDs)
//   NEO_KHZ400  400 KHz (classic 'v1' (not v2) FLORA pixels, WS2811 drivers)
//   NEO_GRB     Pixels are wired for GRB bitstream (most NeoPixel products)
//   NEO_RGB     Pixels are wired for RGB bitstream (v1 FLORA pixels, not v2)
Adafruit_NeoPixel strip = Adafruit_NeoPixel(150, PIN, NEO_GRB + NEO_KHZ800);

// IMPORTANT: To reduce NeoPixel burnout risk, add 1000 uF capacitor across
// pixel power leads, add 300 - 500 Ohm resistor on first pixel's data input
// and minimize distance between Arduino and first pixel.  Avoid connecting
// on a live circuit...if you must, connect GND first.

int ledPattern[] = { 1, 0, 0, 1, 1, 1, 0, 0, 1, 1 };

const uint32_t bgColor = strip.Color(0, 0, 4);
const uint32_t codeColor = strip.Color(0, 63, 0);

// Setup the Analog input pins to serve as digital, switch inputs.
int Ain0 = A0;         // Analog input 0
int Ain1 = A1;         // Analog input 1
int Ain2 = A2;         // Analog input 2
int Ain3 = A3;         // Analog input 3
int Ain4 = A4;         // Analog input 4
int Ain5 = A5;         // Analog input 5
int Ain6 = A6;         // Analog input 6
int Ain7 = A7;         // Analog input 7

int sensorPin = A0;    // select the input pin for the potentiometer
int sensorValue = 0;   // variable to store the value coming from the sensor
int brightness = 0;    // how bright the LED is
int fadeAmount = 5;    // how many points to fade the LED by
int blinkBox = 0;      // Flag to enable blinking of box LED, blinks while > 0

void setup() {

  ///////////////////////////
  // Setting a pin for output
  // pinMode(mapLed, OUTPUT);
  // digitalWrite(10, HIGH);
  //
  // const int sw1in = 1;
  // pinMode(sw1in, INPUT);
  // digitalRead(sw1in);
  ///////////////////////////

  // pinMode(sw1in, INPUT);
  pinMode(sw2in, INPUT);
  pinMode(sw3in, INPUT);
  pinMode(sw4in, INPUT);
      
  // set the house light initially off and not blinking
  blinkBox = 0;


  strip.begin();
  // Set all pixels to the background color to start with
  colorWipe(bgColor, 10); // Start with all off
  strip.show(); // Initialize all pixels to 'off'
}


void loop() {

  // Set all pixels to the background color to start with
  colorWipe(bgColor, 1); // Start with all off
  
  // Wait for a trigger to begin the sequence
  while( digitalRead(sw2in) ) {

    if ( blinkBox > 0 ) {

      // Set the brightness of the map LED, but only after the chart LED
      // has already cycled a couple times.  Causes the map LED to appear
      // to work after a delay.
      if ( blinkBox < 15 ) {
        analogWrite(mapLed, brightness);
      }
      
      // set the brightness of the chart LED
      //analogWrite(chartLed, brightness);

      // change the brightness for next time through the loop:
      brightness = brightness + fadeAmount;

      // reverse the direction of the fading at the ends of the fade:
      if (brightness == 0 || brightness == 255) {
        fadeAmount = -fadeAmount;
        blinkBox--;
      }
      delay(10);
    } else {

    }

    delay(10);
  }

  // read the value from the sensor:
  sensorValue = analogRead(sensorPin);

  // Set all pixels to the background color to start with
  colorWipe(bgColor, 1); // Start with all off
  //digitalWrite(10, HIGH);

  // Advance the initial pattern
  initialPattern(400);

  // Advance the initial pattern
  transmitPattern(150);

  // Designate which box was pulled.
  blinkBox = 20;

  // Do the followup light show if the switch is so enabled
  if( digitalRead(sw3in) ) { 
  
    // Some example procedures showing how to display to the pixels:
    colorWipe(strip.Color(32, 0, 0), 10); // Red
    colorWipe(strip.Color(0, 32, 0), 10); // Green
    colorWipe(strip.Color(0, 0, 32), 10); // Blue
  
    // Send a theater pixel chase in...
    theaterChase(strip.Color(32, 32, 32), 20); // White
    theaterChase(strip.Color(32, 0, 0), 20); // Red
    theaterChase(strip.Color(0, 0, 32), 20); // Blue

    rainbow(20);
    rainbowCycle(20);
    theaterChaseRainbow(50);
  }
}


// Fill the dots one after the other with a color
void colorWipe(uint32_t c, uint8_t wait) {
  for(uint16_t i=0; i<strip.numPixels(); i++) {
    strip.setPixelColor(i, c);
    strip.show();
    delay(wait);
  }
}


void rainbow(uint8_t wait) {
  uint16_t i, j;

  for(j=0; j<256; j++) {
    for(i=0; i<strip.numPixels(); i++) {
      strip.setPixelColor(i, Wheel((i+j) & 255));
    }
    strip.show();
    delay(wait);
  }
}


// Slightly different, this makes the rainbow equally distributed throughout
void rainbowCycle(uint8_t wait) {
  uint16_t i, j;

  for(j=0; j<256*5; j++) { // 5 cycles of all colors on wheel
    for(i=0; i< strip.numPixels(); i++) {
      strip.setPixelColor(i, Wheel(((i * 256 / strip.numPixels()) + j) & 255));
    }
    strip.show();
    delay(wait);
  }
}


//Theatre-style crawling lights.
void theaterChase(uint32_t c, uint8_t wait) {
  for (int j=0; j<10; j++) {  //do 10 cycles of chasing
    for (int q=0; q < 3; q++) {
      for (int i=0; i < strip.numPixels(); i=i+3) {
        strip.setPixelColor(i+q, c);    //turn every third pixel on
      }
      strip.show();
      delay(wait);

      for (int i=0; i < strip.numPixels(); i=i+3) {
        strip.setPixelColor(i+q, 0);        //turn every third pixel off
      }
    }
  }
}


//Theatre-style crawling lights with rainbow effect
void theaterChaseRainbow(uint8_t wait) {
  for (int j=0; j < 256; j++) {     // cycle all 256 colors in the wheel
    for (int q=0; q < 3; q++) {
      for (int i=0; i < strip.numPixels(); i=i+3) {
        strip.setPixelColor(i+q, Wheel( (i+j) % 255));    //turn every third pixel on
      }
      strip.show();
      delay(wait);

      for (int i=0; i < strip.numPixels(); i=i+3) {
        strip.setPixelColor(i+q, 0);        //turn every third pixel off
      }
    }
  }
}


// Input a value 0 to 255 to get a color value.
// The colours are a transition r - g - b - back to r.
uint32_t Wheel(byte WheelPos) {
  WheelPos = 255 - WheelPos;
  if(WheelPos < 85) {
    return strip.Color(255 - WheelPos * 3, 0, WheelPos * 3);
  }
  if(WheelPos < 170) {
    WheelPos -= 85;
    return strip.Color(0, WheelPos * 3, 255 - WheelPos * 3);
  }
  WheelPos -= 170;
  return strip.Color(WheelPos * 3, 255 - WheelPos * 3, 0);
}

/*
// Fill the dots one after the other with a color
void transmitPattern(uint8_t wait) {
  for(uint16_t i=10; i<160; i++) {  
    strip.setPixelColor(i, codeColor);
    strip.setPixelColor(i-1, bgColor);
    strip.setPixelColor(i-2, bgColor);
    strip.setPixelColor(i-3, codeColor);
    strip.setPixelColor(i-4, codeColor);
    strip.setPixelColor(i-5, codeColor);
    strip.setPixelColor(i-6, bgColor);
    strip.setPixelColor(i-7, bgColor);
    strip.setPixelColor(i-8, codeColor);
    strip.setPixelColor(i-9, codeColor);
    strip.setPixelColor(i-10, bgColor);
    strip.show();

    // Cause the pattern to gradually speed up as it goes.
    if (i < 50)       delay(50);
    else if (i < 100) delay(20);
    else              delay(5);  // Decrease delay, accelerate pattern as it goes.
  }
}
*/


// Fill the dots one after the other with a color
void transmitPattern(uint8_t wait) {
  int z = 0;
  for(uint16_t i=0; i <= 149; i++) {
    z = 149 - i;
    strip.setPixelColor(z, bgColor);
    strip.setPixelColor(z-1, codeColor);
    strip.setPixelColor(z-2, codeColor);
    strip.setPixelColor(z-3, bgColor);
    strip.setPixelColor(z-4, bgColor);
    strip.setPixelColor(z-5, codeColor);
    strip.setPixelColor(z-6, codeColor);
    strip.setPixelColor(z-7, codeColor);
    strip.setPixelColor(z-8, bgColor);
    strip.setPixelColor(z-9, bgColor);
    strip.setPixelColor(z-10, codeColor);
    strip.show();

    // Cause the pattern to gradually speed up as it goes.
    if (z > 100)      delay(50);
    else if (z > 50)  delay(20);
    else              delay(5);  // Decrease delay, accelerate pattern as it goes.
  }
}


/*
// Fill the dots one after the other with a color left to right
void initialPattern(uint16_t wait) {

  // Try to build the pattern algorythmetically:
  int z = 0;
  for(int x=0; x<10; x++) {
    for(int y=0; y<10; y++) {
      z = x-y;
      if (z >= 0 ) {
        if( ledPattern[y] == 1 ) {
          strip.setPixelColor(z, codeColor);
        } else {
          strip.setPixelColor(z, bgColor);
        }
      }
    }
    strip.show();  
    delay(wait);
  }
  delay(4 * wait);  
}
*/


// Fill the dots one after the other with a color Right to Left
void initialPattern(uint16_t wait) {

  // for(uint16_t i=0; i<strip.numPixels(); i++)
  // Try to build the pattern algorythmetically:
  int n = 149;
  int z = 0;
  for(int x=0; x<10; x++) {
    for(int y=0; y<10; y++) {
      //z = n-x;   // Left to Right
      z = n-x+y;   // Right to left
      if (y <= x ) {
        if( ledPattern[y] == 1 ) {
          strip.setPixelColor(z, codeColor);
        } else {
          strip.setPixelColor(z, bgColor);
        }
      }
    }
    strip.show();  
    delay(wait);
  }
  delay(4 * wait);  
}

