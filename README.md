# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

This project is inteneded to be a Neo-Pixel-based Andon alert system:
  https://www.adafruit.com/category/168

The idea is to use a generic Arduino processor to drive 1 or more NeoPixel squares, (8x8 blocks)
and use that as a status display in the factory.

The Arudino will be triggered by an external process control device, (PLC), and will generate a 
small series of patterns depending on the input from the process control device:

[||PLC||]----(4 DIO Lines)---->[Arduino]----(DO Signal Line)-->[8x8 Neopixel]
       

### How do I get set up? ###

* Summary of set up
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

Richard Kowalsky - marsbase1@gmail.com
* Other community or team contact